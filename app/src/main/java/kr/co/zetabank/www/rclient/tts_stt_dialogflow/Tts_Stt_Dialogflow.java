package kr.co.zetabank.www.rclient.tts_stt_dialogflow;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.polly.AmazonPollyPresigningClient;
import com.amazonaws.services.polly.model.DescribeVoicesRequest;
import com.amazonaws.services.polly.model.DescribeVoicesResult;
import com.amazonaws.services.polly.model.OutputFormat;
import com.amazonaws.services.polly.model.SynthesizeSpeechPresignRequest;
import com.amazonaws.services.polly.model.Voice;
import com.google.gson.JsonElement;
import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import ai.api.AIListener;
import ai.api.android.AIConfiguration;
import ai.api.model.AIError;
import ai.api.model.AIResponse;
import ai.api.model.Result;
import ai.api.ui.AIButton;
import kr.co.zetabank.www.rclient.LASER_OP;
import kr.co.zetabank.www.rclient.R;

public class Tts_Stt_Dialogflow extends AppCompatActivity implements AIListener, AIButton.AIButtonListener {

    protected static AIButton aiButton;
    //----------------------------------------토큰 설정---------------------------------------------
    // dialogflow 토큰 값
    public String ACCESS_TOKEN_ko = "ba7607416b164901910ced9baaad337e";  // ko
    public String ACCESS_TOKEN_en = "03ef6a473fb542588a1bcf64d32c15f1";  // en
    public String ACCESS_TOKEN_ja = "cd70b07d863544f485673b1698227207";  // ja
    public String ACCESS_TOKEN_cn = "0a6dd1ffc25842359f2f3095a69fdef4";  // zh-Hans

    // STT (textToSpeech)
    public static TextToSpeech textToSpeech;

    // TTS (Amazon Polly)
    String TAG = "PollyDemo";
    String KEY_VOICES = "Voices";
    String COGNITO_POOL_ID = "us-east-2:a54a67ea-8c70-4e13-ac24-b51276fa1f5b";
    Regions MY_REGION = Regions.US_EAST_2;

    // LASER_OP (오케이제타뱅크 음성 인식)
    public static LASER_OP test = null;
    public static int rMode=0;
    public static int Control = 0;
    public static boolean aiButton_auto = false;

    //----------------------------------------------------------------------------------------------
    private static AmazonPollyPresigningClient client;
    private static URL presignedSynthesizeSpeechUrl = null;
    public static MediaPlayer mediaPlayer = null;
    private List<Voice> voices;
    private CognitoCachingCredentialsProvider credentialsProvider;
    public static Result result;
    public static String parameterString;
    public static String textToRead;

    public void TTS_Data(TextView data){
        aiButton = (AIButton) findViewById(R.id.micButton);
        textToRead =data.getText().toString();
    }
    public static SynthesizeSpeechPresignRequest tts_start(String name) {
        // TTS 데이터 설정. ------------------------------------------------------------------------

        //------------------------------------------------------------------------------------------
        if (textToRead.trim().isEmpty()) { }
        SynthesizeSpeechPresignRequest synthesizeSpeechPresignRequest =
                new SynthesizeSpeechPresignRequest()
                        .withText(textToRead)
                        .withVoiceId(name)
                        .withOutputFormat( OutputFormat.Mp3);
        return synthesizeSpeechPresignRequest;
    }
    public static URL country(String name){
        presignedSynthesizeSpeechUrl =
                client.getPresignedSynthesizeSpeechUrl(Tts_Stt_Dialogflow.tts_start(name));
        return presignedSynthesizeSpeechUrl;
    }
    public static void mediaPlayer_stop(){
        mediaPlayer.release();
        mediaPlayer=null;
    }
    // implements AI all----------------------------------------------------------------------------
    @Override
    public void onResult(final AIResponse response) {
        // Get parameters
        parameterString = "";
        if (result.getParameters() != null && !result.getParameters().isEmpty()) {
            for (final Map.Entry<String, JsonElement> entry : result.getParameters().entrySet()) {
                parameterString += "(" + entry.getKey() + ", " + entry.getValue() + ") ";
            }
        }
    }
     @Override
    public void onError(AIError error) {
        LASER_OP.LASER_stop = true;
        LASER_OP.Control = 0;
        Control = 0;
        stt_Start(getApplicationContext());
    }
    public void text_data(TextView data){
        if(data.getText().toString().equals( "Korean" )){
            country( "Seoyeon" );
        }else if(data.getText().toString().equals( "English" )){
            country( "Joanna" );
        }else if(data.getText().toString().equals( "Japanese" )){
            country( "Mizuki" );
        }else if(data.getText().toString().equals( "Chinese" )){
            country( "Zhiyu" );
        }
    }

    //----------------------------------------------------------------------------------------------

    // implements AIListener------------------------------------------------------------------------
    @Override
    public void onAudioLevel(float level) { }
    @Override
    public void onListeningStarted() { }
    @Override
    public void onListeningCanceled() { }
    @Override
    public void onListeningFinished() { }
    //----------------------------------------------------------------------------------------------

    // implements AIButton.AIButtonListener---------------------------------------------------------
    @Override
    public void onCancelled() {}
    //----------------------------------------------------------------------------------------------


    // TextToSpeech---------------------------------------------------------------------------------
    public static void stt_Start(final Context context) {
        if (textToSpeech == null) {
            textToSpeech = new TextToSpeech(context, new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int i) {
                    if (i == TextToSpeech.SUCCESS) {
                        Log.e("main","sssd");
                        textToSpeech.setLanguage( Locale.KOREA);
                    }else{
                        Log.e("main",i+"d");
                    }
                }
            });
        }
    }
    //----------------------------------------------------------------------------------------------


    public static AIConfiguration config(AIConfiguration.SupportedLanguages name, String name1 ){
        AIConfiguration config = new AIConfiguration(name1,
                name,
                AIConfiguration.RecognitionEngine.System);
        return config;
    }
    public static TextView View_name(TextView name){
        return name;
    }
    void setupVoicesSpinner() {
        new GetPollyVoices().execute();
    }
    public class GetPollyVoices extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            if (voices != null) {
                return null;
            }
            DescribeVoicesRequest describeVoicesRequest = new DescribeVoicesRequest();
            DescribeVoicesResult describeVoicesResult;
            try {
                describeVoicesResult = client.describeVoices( describeVoicesRequest );
            } catch (RuntimeException e) {
                Log.e( TAG, "Unable to get available voices. " + e.getMessage() );
                return null;
            }
            // Get list of voices from the result.
            voices = describeVoicesResult.getVoices();
            // Log a message with a list of available TTS voices.
            Log.i( TAG, "Available Polly voices: " + voices );
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if (voices == null) {
                return;
            }
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setupVoicesSpinner();
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(KEY_VOICES, (Serializable) voices);
        super.onSaveInstanceState(outState);
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        voices = (List<Voice>) savedInstanceState.getSerializable(KEY_VOICES);
    }
    public void setupNewMediaPlayer() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.release();
                setupNewMediaPlayer();
                LASER_OP.LASER_stop = true;
                LASER_OP.Control = 0;
                Control = 0;
            }
        });
        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.start();
            }
        });
        mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                return false;
            }
        });
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
    public void initPollyClient() {
        credentialsProvider = new CognitoCachingCredentialsProvider(
                getApplicationContext(),
                COGNITO_POOL_ID,
                MY_REGION
        );
        client = new AmazonPollyPresigningClient(credentialsProvider);
    }
    public void mediaPlayer_State() {
        try {
            if (mediaPlayer.isPlaying()) {
                setupNewMediaPlayer();
            }
        }catch (Exception e){
            setupNewMediaPlayer();
        }
        mediaPlayer.setAudioStreamType( AudioManager.STREAM_MUSIC );
        try {
            mediaPlayer.setDataSource( presignedSynthesizeSpeechUrl.toString() );
        } catch (IOException e) {
//            Log.e( TAG, "Unable to set data source for the media player! " + e.getMessage() );
        }
        mediaPlayer.prepareAsync();
    }
    // RECORD_AUDIO 권한 주기
    public void RECORD_AUDIO(){
        // OS가 Marshmallow 이상일 경우 권한체크를 해야 합니다.
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission( this, Manifest.permission.RECORD_AUDIO ) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions( this, new String[]{Manifest.permission.RECORD_AUDIO}, 1000 );
            } else {
                // READ_EXTERNAL_STORAGE 에 대한 권한이 있음.
            }
        }else {
            // OS가 Marshmallow 이전일 경우 권한체크를 하지 않는다.
        }
    }
    // RECORD_AUDIO 권한 주기
    public void READ_PHONE_STATE(){
        // OS가 Marshmallow 이상일 경우 권한체크를 해야 합니다.
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission( this, Manifest.permission.READ_PHONE_STATE  ) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions( this, new String[]{Manifest.permission.READ_PHONE_STATE }, 1000 );
            } else {
                // READ_EXTERNAL_STORAGE 에 대한 권한이 있음.
            }
        }else {
            // OS가 Marshmallow 이전일 경우 권한체크를 하지 않는다.
        }
    }
    // RECORD_AUDIO 권한 주기
    public void WRITE_EXTERNAL_STORAGE(){
        // OS가 Marshmallow 이상일 경우 권한체크를 해야 합니다.
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission( this, Manifest.permission.WRITE_EXTERNAL_STORAGE  ) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions( this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE }, 1000 );
            } else {
                // READ_EXTERNAL_STORAGE 에 대한 권한이 있음.
            }
        }else {
            // OS가 Marshmallow 이전일 경우 권한체크를 하지 않는다.
        }
    }
    // LASER_OP_시작
    public void LASER_OP_STARE(){
        rMode=0;
        LASER_OP.isForceToFinish = false;
        test = new LASER_OP();
        test.start(); // 이 메소드를 실행하면 Thread 내의 run()을 수행한다.
    }
    public void Aibutton_auto(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    if (aiButton_auto == true) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (Control == 0) {
                                    System.out.println("aiButton_auto 누르기");
                                    Control = 1;
                                    aiButton.performClick();
                                    aiButton_auto = false;
                                }
                            }
                        });
                    }
                }
            }
        }).start();
    }
}
