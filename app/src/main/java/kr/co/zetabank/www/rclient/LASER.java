package kr.co.zetabank.www.rclient;

public class LASER {
    // 초기화
    public LASER() {}

    /// LASER객체를 생성하고, 동작에 필요한 메모리를 할당한다.
    /// 인식기의 기본 옵션을 세팅한다.
    /// @return 생성된 객체의 포인터를 반환한다. 에러가 있으면 NULL을 반환
    public native int Create(String szBaseDir);
    //
    /// LASER객체를 생성하고, 동작에 필요한 메모리를 할당한다.
    /// 인식기의 기본 옵션을 세팅한다.
    /// 추가로 create 시에 사용하는 옵션을 인자로 받는다.
    /// @return 생성된 객체의 포인터를 반환한다. 에러가 있으면 NULL을 반환
    public native int CreateExt(String szBaseDir, int flag);

    /// 생성된 LASER객체를 해제한다.
    public native int Free();

    /// 인식 도메인을 준비한다.
    /// 해당 도메인의 인식에 필요한 리소스 파일들을 로드하고, 메모리를 할당한다.
    /// @param pxLASER 생성된 LASER 객체의 포인터
    /// @param szLexicon 설정하고자하는 도메인의 이름
    /// @param iMode 0으로 고정하여 사용한다.
    public native int Prepare(String szLexicon, int iMode);

    /// PrepareLASER 함수에서 할당한 리소스를 해제한다.
    public native int Unprepare();

    /// 인식 모드를 설정한다.
    /// 0: 주소인식
    /// 2: 대용량 POI 인식
    public native void SetRecMode( int iMode );

    /// 발화 단위의 초기화를 수행한다.
    /// 새로운 발화에 대해서 인식을 수행하기 전에 호출해준다. 내부적으로 사용되는
    /// 변수값을 모두 초기화한다.
    /// @param pxLASER 생성된 LASER 객체의 포인터
    public native int Reset();

    /// 실제 인식을 수행한다.
    /// 매 프레임마다 입력된 음성신호로부터 인식을 수행한다.
    /// 입력신호의 길이가 너무 긴 경우 오류를 발생시킬 수 있다. 3200 이하를 권장.
    /// LASER Event를 반환한다.
    public native int Recognize(int a_size, short[] arr);

    /// 인식결과를 받아온다.
    public native byte[] GetResult(int nBest);

    public native int SetOption( String key, String val );
    public native int SetFrontEndOption( String key, String val );
    public native int SetLogFile( String fname );

    static {
        System.loadLibrary("voicecmd_laser");
    }
}
