package kr.co.zetabank.www.rclient;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import ai.api.android.AIConfiguration;
import ai.api.model.AIError;
import ai.api.model.AIResponse;
import kr.co.zetabank.www.rclient.tts_stt_dialogflow.Tts_Stt_Dialogflow;

public class MainActivity extends Tts_Stt_Dialogflow {

    private Button ko, en, ja, zh_Hans, stop;
    public static TextView tv;
    public static EditText tv1, tv2;
    public static EditText tv3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );
        tv = (TextView) findViewById(R.id.textView);
        tv1 = (EditText) findViewById(R.id.textView1);
        tv2 = (EditText) findViewById(R.id.textView2);
        tv3 = (EditText) findViewById(R.id.textView3);
        ko = (Button)findViewById( R.id.button1 );
        en = (Button)findViewById( R.id.button2 );
        ja = (Button)findViewById( R.id.button3 );
        zh_Hans = (Button)findViewById( R.id.button4 );
        stop = (Button)findViewById( R.id.button_stop );
        TTS_Data(tv3);

        //------------------------------------동적권한----------------------------------------------
        RECORD_AUDIO();
        READ_PHONE_STATE();
        WRITE_EXTERNAL_STORAGE();
        //------------------------------------------------------------------------------------------

        initPollyClient();
        setupNewMediaPlayer();
        ko.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aiButton.initialize(Tts_Stt_Dialogflow.config(AIConfiguration.SupportedLanguages.Korean, ACCESS_TOKEN_ko));
                tv.setText("Korean");
            }
        });
        en.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aiButton.initialize(Tts_Stt_Dialogflow.config(AIConfiguration.SupportedLanguages.English, ACCESS_TOKEN_en));
                tv.setText("English");
            }
        });
        ja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aiButton.initialize(Tts_Stt_Dialogflow.config(AIConfiguration.SupportedLanguages.Japanese, ACCESS_TOKEN_ja));
                tv.setText("Japanese");
            }
        });
        zh_Hans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aiButton.initialize(Tts_Stt_Dialogflow.config(AIConfiguration.SupportedLanguages.ChineseChina, ACCESS_TOKEN_cn));
                tv.setText("Chinese");
            }
        });
        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        tv.setText("Korean");
        aiButton.initialize(Tts_Stt_Dialogflow.config(AIConfiguration.SupportedLanguages.Korean, ACCESS_TOKEN_ko));
        MainActivity.aiButton.setResultsListener( this );
        LASER_OP_STARE();
        Aibutton_auto();
    }
    @Override
    public void onResult(final AIResponse response) {
        result = response.getResult();
        tv1.setText(result.getResolvedQuery());
        tv2.setText(parameterString);
        tv3.setText(response.getResult().getFulfillment().getSpeech());
        text_data(tv);
        mediaPlayer_State();
        }
    @Override
    public void onError(AIError error) {
        tv1.setText( "다시 말해 주세요." );
    }
}
