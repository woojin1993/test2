package kr.co.zetabank.www.rclient;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Environment;
import android.util.Log;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import ai.api.ui.AIButton;
import kr.co.zetabank.www.rclient.tts_stt_dialogflow.Tts_Stt_Dialogflow;

public class LASER_OP extends Thread {
    public static LASER laser = new LASER();
    public String sdcard_dir = null;
    static boolean isInitialized = false;
    int utteranceIdx = 0;
    public static AudioRecord audioRecord;
    final static int frequency = 16000;
    final static int channel_conf = AudioFormat.CHANNEL_CONFIGURATION_MONO;
    final static int audioEncoding = AudioFormat.ENCODING_PCM_16BIT;
    float f;
    public static boolean LASER_stop = true;
    public static int Control = 0;

    // recording 관련
    Boolean isRecording = false; // recording 상태
    Boolean isDeviceOpen = false; // recording device 상태
    public static Boolean isForceToFinish = false;


    String InitializeStorage() {
        // 외부 메모리 상태 확인
        Boolean isExternal = Environment.getExternalStorageState().equals(
                android.os.Environment.MEDIA_MOUNTED );

        if (isExternal == true) {
            System.out.println( "InitializeStorage() ok. SDCARD 이상 없음.\n" );
            System.out.println(Environment.getExternalStorageDirectory().getAbsolutePath());
            return Environment.getExternalStorageDirectory().getAbsolutePath();

        } else {
            Log.i( "STORAGE", "NO EXIST SDCARD" );
            System.out.println( "*ERROR* SDCARD가 필요합니다.\n" );
            return "";
        }
    }

    public void recordAndRecog() {
        if(LASER_stop==true){
            System.out.println("isForceToFinish");
            // 느린 기기일 경우나, 음성이 끊길 경우 주석을 풀고 쓰도록 한다.
            android.os.Process.setThreadPriority( android.os.Process.THREAD_PRIORITY_URGENT_AUDIO );
            audioRecord = null;
            utteranceIdx++;
            try {
                @SuppressWarnings("unused")
                int event = 0;
                int bufferSize = AudioRecord.getMinBufferSize( frequency, channel_conf, audioEncoding );
                bufferSize = 16000;
                // 배열 초기화
                short[] inputBuffer = new short[bufferSize];
                byte[] byteBuffer = new byte[bufferSize * 2];
                Log.i( "LASER", "recording starts" );
                // 마이크 입력 설정
                audioRecord = new AudioRecord( MediaRecorder.AudioSource.VOICE_RECOGNITION,
                        frequency, channel_conf, audioEncoding, bufferSize );
                // PCM 데이터를 파일로 쓰고 싶을 경우 주석을 풀고 쓰도록 한다.
                SimpleDateFormat mDateFormat = new SimpleDateFormat( "MMddHHmmss", Locale.KOREA );
                Date currentTime = new Date();
                String mTime = mDateFormat.format( currentTime );
                String infname = "input" + mTime +
                        String.format( "_%d", utteranceIdx ) + ".pcm";
                Log.i( "LASER", "file saving done" );
                laser.Reset();
                // 마이크 입력 시작
                audioRecord.startRecording();
                // 파일 열기 및 인덱스 초기화
                isDeviceOpen = isRecording = true;
                isForceToFinish = true;
                while (isForceToFinish == true) {
                    int inBufSize = 320 * 2;
                    int bufferReadResult = audioRecord.read( inputBuffer, 0, inBufSize );
                    if (bufferReadResult > 0) {
                        ByteBuffer.wrap( byteBuffer ).order(
                                ByteOrder.LITTLE_ENDIAN ).asShortBuffer().put( inputBuffer );
                    }
                    event = laser.Recognize( bufferReadResult, inputBuffer );
                    if ((event & 0x0001) > 0)
                        System.out.println( "Recognized\n" );
                    else if ((event & 0x0002) > 0)
                        System.out.println( "Time Out\n" );
                    else if ((event & 0x0004) > 0)
                        System.out.println( "Rejected\n" );
                    else if ((event & 0x0008) > 0)
                        System.out.println( "User Break\n" );
                    else
                        continue;
                    break;
                }
                //dos.close();
                if (isDeviceOpen == true ) {
                    // audio input device 해제
                    audioRecord.stop();
                    audioRecord.release();
                    isDeviceOpen = false;
                    isForceToFinish = false;
                }
                // 이제 인식 결과 출력
                if ((event & 0x0001) > 0) {
                    String recog_out = new String( laser.GetResult( 1 ), "MS949" );
                    if (recog_out.length() > 1) {
                        String result = recog_out.split( ";" )[0].replaceAll( "[^\uAC00-\uD7AF\u1100-\u11FF\u3130-\u318F]", "" );
                        f = Float.parseFloat( recog_out.split( ";" )[1].substring( 0, 8 ) );   // 32비트 실수로 그리고 1을 더하기
                        System.out.println( result + " : " + (f * 100) + "%" );
                    }
                } else {
                    System.out.print( "" );
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            // LASER_OP stop = true 일때 인식 안함.
        }
        if(f>0.8) {
            if(Control == 0) {
                LASER_stop = false;
                Tts_Stt_Dialogflow.aiButton_auto = true;
                Control = 1;
            }
        }
    }
    public boolean InitEngine(String dbname) {
        sdcard_dir = InitializeStorage();
        int ret;
        Log.i( "LASER", "BEGIN CREATE LASER" + sdcard_dir );
        ret = laser.Create( sdcard_dir + dbname );
        if (ret != 0)
            Log.i( "LASER", "ERROR WHILE CREATELASER" );
        else
            Log.i( "LASER", "INIT LASER DONE" );
        ret = laser.Prepare( "asr_kor", 0 );
        ret = laser.SetOption( "LASER_LOGFILENAME",
                sdcard_dir + "/Laser/LOG/log.txt" );
        laser.Reset();
        if (ret == 0)
            Log.i( "LASER", "SET LOGFILE DONE" );
        else
            Log.i( "LASER", "ERROR WHILE SET LOGFILE" );
        Log.i( "LASER", "INIT LASER ALL DONE" );
        return true;
    }
    public void run() {
        try {
            if (isInitialized == false) {
                isInitialized = InitEngine("/Laser/LASERDB.mn");
            }
            while (true) {
                recordAndRecog();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
